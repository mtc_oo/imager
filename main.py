import os
from PIL import Image

def getImageFilesUnderDir(myDir):
	''' creates list of files for extensions jpg, jpeg, png '''
	extList = ['.jpg', '.jpeg', '.png']
	files = os.listdir(myDir)
	imageFileList = []
	for i in files:
		filename, fileext = os.path.splitext(i)
		if fileext in extList:
			imageFileList.append(i)
	return imageFileList

def reduceImageSize(PILimage, proportion):
	mySize = PILimage.size
	toSize0 = int(round(mySize[0]*1.0/proportion, 0))
	toSize1 = int(round(mySize[1]*1.0/proportion, 0))
	toIm = PILimage.resize((toSize0,toSize1))
	return toIm


inDir = "./in/"
outDir = "./out/"
iList = getImageFilesUnderDir(inDir)
print iList
for i in iList:
	im = Image.open(inDir+i)
	nim = reduceImageSize(im,4)
	nim.save(outDir+'r4'+i)



